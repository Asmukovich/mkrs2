﻿using System;
using System.Collections.Generic;

public class Edge : IComparable<Edge>
{
    public int Source;
    public int Destination;
    public int Weight;

    public int CompareTo(Edge other)
    {
        return Weight.CompareTo(other.Weight);
    }
}

public class KruskalAlgorithm
{
    private int verticesCount;
    private List<Edge> edges = new List<Edge>();

    public KruskalAlgorithm(int verticesCount)
    {
        this.verticesCount = verticesCount;
    }

    public void AddEdge(int source, int destination, int weight)
    {
        edges.Add(new Edge { Source = source, Destination = destination, Weight = weight });
    }

    private int Find(int[] parent, int i)
    {
        if (parent[i] == -1)
            return i;
        return Find(parent, parent[i]);
    }

    private void Union(int[] parent, int x, int y)
    {
        int xset = Find(parent, x);
        int yset = Find(parent, y);
        parent[xset] = yset;
    }

    public void Kruskal()
    {
        List<Edge> result = new List<Edge>();
        int i = 0;
        int e = 0;
        edges.Sort();

        int[] parent = new int[verticesCount];
        for (int v = 0; v < verticesCount; ++v)
            parent[v] = -1;

        while (e < verticesCount - 1)
        {
            Edge nextEdge = edges[i];
            i++;

            int x = Find(parent, nextEdge.Source);
            int y = Find(parent, nextEdge.Destination);

            if (x != y)
            {
                result.Add(nextEdge);
                Union(parent, x, y);
                e++;
            }
        }

        Console.WriteLine("Мiнiмальне охоплення графа:");
        foreach (Edge edge in result)
        {
            Console.WriteLine("{0} - {1}: {2}", edge.Source, edge.Destination, edge.Weight);
        }
    }

    public List<Edge> GetEdges()
    {
        return edges;
    }

    public int GetVerticesCount()
    {
        return verticesCount;
    }

    public int GetEdgesCount()
    {
        return edges.Count;
    }
}

public class Program
{
    public static void Main()
    {
        KruskalAlgorithm kruskal = new KruskalAlgorithm(6);

        // Додавання початкових ребер
        kruskal.AddEdge(0, 1, 2);
        kruskal.AddEdge(0, 2, 3);
        kruskal.AddEdge(1, 2, 1);
        kruskal.AddEdge(1, 3, 1);
        kruskal.AddEdge(2, 3, 2);
        kruskal.AddEdge(3, 4, 1);
        kruskal.AddEdge(3, 5, 4);
        kruskal.AddEdge(4, 5, 3);

        // Виконання алгоритму Крускала
        kruskal.Kruskal();

        // Виведення інформації про граф
        Console.WriteLine("\nIнформацiя про Граф:");
        Console.WriteLine($"Кiлькiсть вершин: {kruskal.GetVerticesCount()}");
        Console.WriteLine($"Кiлькiсть ребер: {kruskal.GetEdgesCount()}");

        // Виведення списку ребер
        Console.WriteLine("\nРебра:");
        foreach (Edge edge in kruskal.GetEdges())
        {
            Console.WriteLine($"{edge.Source} - {edge.Destination}: {edge.Weight}");
        }

        Console.ReadLine();
    }
}
